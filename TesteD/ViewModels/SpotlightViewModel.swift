//
//  SpotlightViewModel.swift
//  TesteD
//
//  Created by Vandecarlos Cavalcanti De Santana on 21/11/20.
//

import Foundation
import UIKit

struct SpotlightListViewModel {
    let spotlights: [Spotlight]
}
extension SpotlightListViewModel {
    var numberOfSections: Int {
        return 1
    }
    
    func numberOfRowsSections(_ section: Int) -> Int {
        return self.spotlights.count
    }
    
    func spotlightAtIndex(_ index:Int) -> SpotlightViewModel {
        let spotlight = self.spotlights[index]
        
        return SpotlightViewModel(spotlight)
    }
}

struct SpotlightViewModel {
    private let spotlight : Spotlight
}

extension SpotlightViewModel {
    init (_ spotlight: Spotlight){
        self.spotlight = spotlight
    }
}
extension SpotlightViewModel {
    var name: String {
        return self.spotlight.name ?? ""
    }
    var  bannerURL: String {
        return self.spotlight.bannerURL ?? ""
    }
    var  description: String {
        return self.spotlight.description ?? ""
    }
    func  assetImage(_ completion: @escaping (UIImage?) -> Void) {
        
        let url = URL(string: self.spotlight.bannerURL ?? "")
        DispatchQueue.global().async  {
            let data = try? Data(contentsOf: url!)
            DispatchQueue.main.async {
                if data != nil {
                    completion(UIImage(data: data!))
                }
                
            }
        }
        
    }
}
