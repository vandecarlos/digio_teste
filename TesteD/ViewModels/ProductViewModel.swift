//
//  ProductViewModel.swift
//  TesteD
//
//  Created by Vandecarlos Cavalcanti De Santana (RESOURCE TECNOLOGIA E INFORMATICA LTDA – GEDES – SP) on 22/11/20.
//

import Foundation
import UIKit

struct ProductListViewModel {
    let products: [Product]
}
extension ProductListViewModel {
    var numberOfSections: Int {
        return 1
    }
    
    func numberOfRowsSections(_ section: Int) -> Int {
        return self.products.count
    }
    
    func productAtIndex(_ index:Int) -> ProductViewModel {
        let product = self.products[index]
        
        return ProductViewModel(product)
    }
}

struct ProductViewModel {
    private let product : Product
}

extension ProductViewModel {
    init (_ product: Product){
        self.product = product
    }
}
extension ProductViewModel {
    var name: String {
        return self.product.name ?? ""
    }
    
    func  assetImage(_ completion: @escaping (UIImage?) -> Void) {
        
        let url = URL(string: self.product.imageURL ?? "")
        DispatchQueue.global().async  {
            let data = try? Data(contentsOf: url!)
            DispatchQueue.main.async {
                if data != nil {
                    completion(UIImage(data: data!))
                }
                
            }
        }
        
    }
    var  description: String {
        return self.product.description ?? ""
    }
}

