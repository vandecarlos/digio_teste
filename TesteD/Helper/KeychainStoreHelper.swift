//
//  KeychainStoreHelper.swift
//  TesteD
//
//  Created by Vandecarlos Cavalcanti De Santana on 21/11/20.
//

import Foundation
import KeychainSwift

class KeychainStoreHelper {
    
    static var shared = KeychainStoreHelper()
    
    func setImage(image: UIImage){
        
        let keychain = KeychainSwift()
        let imageB64 = Base64().convertImageToBase64(image: image)
        keychain.set(imageB64, forKey: "imageB64")
    }
    
    func getImage() -> UIImage {
        
        let keychain = KeychainSwift()
        
        let imB64:String =  String(keychain.get("imageB64")!)
        
        return Base64().convertBase64ToImage(imageString: imB64)

    }
    
}
