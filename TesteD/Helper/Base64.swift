//
//  Base64.swift
//  TesteD
//
//  Created by Vandecarlos Cavalcanti De Santana on 21/11/20.
//

import Foundation
import UIKit

class Base64 {
    
    func codificarStringBase64(texto:String) -> String {
        let dados = texto.data(using: String.Encoding.utf8)
        let base64 = dados!.base64EncodedString()
        
        return base64
    }
    
    func convertImageToBase64(image: UIImage) -> String {
        let imageData = image.jpegData(compressionQuality: 0.1)
        if let stri = imageData?.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters){
            return stri as String
        }
        return ""
      
    }
    

    func convertBase64ToImage(imageString: String) -> UIImage {
        let imageData = Data(base64Encoded: imageString, options: Data.Base64DecodingOptions.ignoreUnknownCharacters)!
        return UIImage(data: imageData)!
    }

}
