//
//  Cash.swift
//  TesteD
//
//  Created by Vandecarlos Cavalcanti De Santana (RESOURCE TECNOLOGIA E INFORMATICA LTDA – GEDES – SP) on 21/11/20.
//

import Foundation


struct Cash: Decodable {
    let title: String?
    let bannerURL: String?
    let description: String?
}
