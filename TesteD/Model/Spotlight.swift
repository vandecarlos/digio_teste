//
//  Spotlight.swift
//  TesteD
//
//  Created by Vandecarlos Cavalcanti De Santana  on 21/11/20.
//

import Foundation

struct Spotlight: Decodable {
    let name: String?
    let bannerURL: String?
    let description: String?
}
