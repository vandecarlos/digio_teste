//
//  Products.swift
//  TesteD
//
//  Created by Vandecarlos Cavalcanti De Santana (RESOURCE TECNOLOGIA E INFORMATICA LTDA – GEDES – SP) on 21/11/20.
//

import Foundation

struct Product: Decodable {
    let name: String?
    let imageURL: String?
    let description: String?
}
