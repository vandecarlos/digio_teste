//
//  SpotlightCollectionViewCell.swift
//  TesteD
//
//  Created by Vandecarlos Cavalcanti De Santana  on 21/11/20.
//

import UIKit

class SpotlightCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var banner:UIImageView!
    @IBOutlet weak var load: UIActivityIndicatorView!
    @IBOutlet weak var viewLoad: UIView!
}
