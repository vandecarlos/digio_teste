//
//  ProductCollectionViewCell.swift
//  TesteD
//
//  Created by Vandecarlos Cavalcanti De Santana  on 22/11/20.
//

import UIKit

class ProductCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var icon:UIImageView!
    @IBOutlet weak var load: UIActivityIndicatorView!
    @IBOutlet weak var viewLoad: UIView!
}
