//
//  HomeViewController.swift
//  TesteD
//
//  Created by Vandecarlos Cavalcanti De Santana  21/11/20.
//

import UIKit

class HomeViewController: UIViewController {
    
    @IBOutlet weak var lbName: UILabel!
    @IBOutlet weak var collectionSpotlight: UICollectionView!
    @IBOutlet weak var collectionProduct: UICollectionView!
    
    @IBOutlet weak var lbSubTitle: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var viewLoad: UIView!

    private var spotlightListVM: SpotlightListViewModel!
    private var productListVM: ProductListViewModel!
    
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.isNavigationBarHidden = true
        self.lbName.text = "Olá, Maria"
        loadData()
    }
    
    private func loadData(){
        spinner.startAnimating()
        self.viewLoad.isHidden = false
        Webservice().getSpotlight(){ result in
            DispatchQueue.main.async {
                if let spotlights = result?.spotlight {
                    self.spotlightListVM = SpotlightListViewModel(spotlights: spotlights)
                    if let products = result?.products {
                        self.productListVM = ProductListViewModel(products: products)
                        if let cast = result?.cash {
                            self.lbSubTitle.text = cast.title
                            let urImage = URL(string:cast.bannerURL ?? "")
                            let data = try? Data(contentsOf: urImage!)
                            self.imageView.image = UIImage(data: data!)
                            
                            self.spinner.stopAnimating()
                            self.collectionSpotlight.reloadData()
                            self.collectionProduct.reloadData()
                            self.viewLoad.isHidden = true
                        }
                    }
                    
                }
                
            }

        }
        
    }

    func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }
}
extension HomeViewController : UICollectionViewDelegate, UICollectionViewDataSource {
   
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if collectionView.tag == 1 {
            return self.spotlightListVM == nil ? 0 : self.spotlightListVM.numberOfSections
        } else {
            return self.productListVM == nil ? 0 : self.productListVM.numberOfSections
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == 1 {
            return self.spotlightListVM.numberOfRowsSections(section)
        }else {
            return self.productListVM.numberOfRowsSections(section)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView.tag == 1 {
            
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SpotlightCollectionViewCell", for: indexPath) as? SpotlightCollectionViewCell else {
                fatalError("SpotlightCollectionViewCell not found")
            }
            let spotlightVM = self.spotlightListVM.spotlightAtIndex(indexPath.row)
            cell.viewLoad.isHidden = false
            cell.load.startAnimating()
            spotlightVM.assetImage { im in
                cell.banner.image  = im
                cell.viewLoad.isHidden = true
                cell.load.startAnimating()
            }
            
            return cell
            
        }else{
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductCollectionViewCell", for: indexPath) as? ProductCollectionViewCell else {
                fatalError("ProductCollectionViewCell not found")
            }
            let productVM = self.productListVM.productAtIndex(indexPath.row)
            
            cell.viewLoad.isHidden = false
            cell.load.startAnimating()
            productVM.assetImage { im in
                cell.icon.image = im
                cell.viewLoad.isHidden = true
                cell.load.stopAnimating()
            }
          
            return cell
        }
        
        
    }
    
    
}
