//
//  Webservice.swift
//  TesteD
//
//  Created by Vandecarlos Cavalcanti De Santana  on 21/11/20.
//

import Foundation

class Webservice {
    
    let url = URL(string: "https://7hgi9vtkdc.execute-api.sa-east-1.amazonaws.com/sandbox/products")!
    func getSpotlight(completion: @escaping (Response?) ->()){
        
        URLSession.shared.dataTask(with: url) { data, response, error in
            
            if let error = error {
                print(error.localizedDescription)
                completion(nil)
            } else if let data = data {
                
                let result = try? JSONDecoder().decode(Response.self, from: data)
                
                if let result = result {
                    completion(result)
                }
                print(result)
                
            }
            
        }.resume()
    }
    
    
}
