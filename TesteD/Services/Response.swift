//
//  Response.swift
//  TesteD
//
//  Created by Vandecarlos Cavalcanti De Santana (RESOURCE TECNOLOGIA E INFORMATICA LTDA – GEDES – SP) on 22/11/20.
//

import Foundation

struct Response: Decodable {
    let spotlight:[Spotlight]?
    let cash:Cash?
    let products: [Product]?
}
